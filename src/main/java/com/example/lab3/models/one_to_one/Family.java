package com.example.lab3.models.one_to_one;

import javax.persistence.*;

@Entity
@Table(name = "family")
public class Family {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "idFamily")
    private int id;

    @Column(name = "FamilyName")
    private String name;

    @OneToOne
    @JoinColumn(name = "kid_id_kids")
    private Kids kid;

    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Kids getKids(){
        return kid;
    }

    public void setKids(Kids car){
        this.kid = car;
    }
}
