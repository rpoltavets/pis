package com.example.lab3.models.one_to_one;

import javax.persistence.*;

@Entity
@Table(name = "kids")
public class Kids {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_kids")
    private int id;

    @Column(nullable = false)
    private String kidsName;

    @OneToOne(mappedBy = "kid")
    private Family users;

    public String getKidsName(){
        return kidsName;
    }

    public void setKidsName(String kidsName){
        this.kidsName = kidsName;
    }
}
