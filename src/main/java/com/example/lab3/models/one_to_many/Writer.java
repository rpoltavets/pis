package com.example.lab3.models.one_to_many;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "writer")
public class Writer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id_writer")
    private int id;

    @Column(name = "writer")
    private String writerName;

    @OneToMany(mappedBy = "writer")
    private Set<Books> book;

    public String getWriterName(){
        return writerName;
    }

    public void setWriterName(String writerName) {
        this.writerName = writerName;
    }
}
