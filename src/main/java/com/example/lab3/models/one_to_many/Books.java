package com.example.lab3.models.one_to_many;

import javax.persistence.*;

@Entity
@Table(name = "book")
public class Books {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_book")
    private int id;

    @Column(name = "book")
    private String bookName;

    @ManyToOne
    @JoinColumn(name = "id_writer")
    private Writer writer;

    public int getId() {
        return id;
    }

    public void setId(int id) { this.id = id; }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) { this.bookName = bookName; }

    public Writer getWriter() {
        return writer;
    }

    public void setWriter(Writer writer) {
        this.writer = writer;
    }
}
