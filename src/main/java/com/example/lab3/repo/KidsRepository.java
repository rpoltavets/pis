package com.example.lab3.repo;

import com.example.lab3.models.one_to_one.Kids;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KidsRepository extends JpaRepository<Kids,Integer> {
}
