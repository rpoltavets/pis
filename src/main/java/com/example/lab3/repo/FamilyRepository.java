package com.example.lab3.repo;

import com.example.lab3.models.one_to_one.Family;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FamilyRepository extends JpaRepository<Family, Integer> {
}