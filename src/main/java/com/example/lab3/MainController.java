package com.example.lab3;

import java.util.List;

import com.example.lab3.models.one_to_many.Books;
import com.example.lab3.models.one_to_one.Family;
import com.example.lab3.repo.BooksRepository;
import com.example.lab3.repo.FamilyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class MainController {
    @Autowired
    FamilyRepository familyRep;

    @GetMapping("/family")
    public List<Family> family() {
        return familyRep.findAll();
    }

    @Autowired
    BooksRepository booksRep;

    @GetMapping("/books")
    public List<Books> books(){
        return booksRep.findAll();
    }
}
