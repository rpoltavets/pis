let block1 = document.getElementsByClassName("text")[0];
let block2 = document.getElementsByClassName("text")[1];
fetch('http://localhost:8080/family')
.then((response) => {
    return response.json();
})
.then((data) => {
        data.forEach(element => {
            //console.log(element);
            block1.innerHTML += "<p>" + element.name +  " ---- " + element.kids.kidsName +  "</p>" ;
        });
});

fetch('http://localhost:8080/books')
.then((response) => {
    return response.json();
})
.then((data) => {
        data.forEach(element => {
            //console.log(element);
            block2.innerHTML += "<p>" + element.writer.writerName+ " ---- " + element.bookName + "</p>";
        });
});